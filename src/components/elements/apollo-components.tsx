export type Maybe<T> = T | null;

export interface OrdersWhereInput {
  deliveryType?: Maybe<string>;

  paymentType?: Maybe<string>;

  deliveryStatus?: Maybe<string>;

  paymentStatus?: Maybe<string>;

  customerName?: Maybe<string>;

  phone?: Maybe<string>;

  number?: Maybe<string>;
}

export interface OrderWhereInput {
  id?: Maybe<number>;

  number?: Maybe<string>;
}

export interface ProductWhereInput {
  id?: Maybe<number>;

  title?: Maybe<string>;

  url?: Maybe<string>;
}

export interface ProductsWhereInput {
  id?: Maybe<number[]>;

  title?: Maybe<string>;

  url?: Maybe<string>;
}

export interface OrderAddInput {
  deliveryType: DeliveryTypes;

  deliveryStatus: DeliveryStatus;

  paymentType: PaymentTypes;

  paymentStatus: PaymentStatus;

  customerName: string;

  email?: Maybe<string>;

  address: AddressInput;

  phone?: Maybe<string>;

  comment?: Maybe<string>;

  cart: CartInput[];
}

export interface AddressInput {
  city: string;

  street: string;

  apartment: string;

  postalCode: string;
}

export interface CartInput {
  id?: Maybe<number>;

  quantity: number;

  dosageId: number;

  productId: number;

  dosageDetailId: number;
}

export interface OrderUpdateInput {
  id: number;

  deliveryType?: Maybe<DeliveryTypes>;

  deliveryStatus?: Maybe<DeliveryStatus>;

  paymentType?: Maybe<PaymentTypes>;

  paymentStatus?: Maybe<PaymentStatus>;

  customerName?: Maybe<string>;

  email?: Maybe<string>;

  address?: Maybe<AddressInput>;

  phone?: Maybe<string>;

  comment?: Maybe<string>;

  cart?: Maybe<CartInput[]>;
}

export interface ProductAddInput {
  title: string;

  url: string;

  logo: FileInput;

  gallery?: Maybe<FileInput[]>;

  withAlcohol: boolean;

  timeToStart: string;

  workTime: string;

  meta?: Maybe<ProductMetaInput[]>;

  dosages: DosageInput[];

  description: string;

  seoTitle: string;

  seoDescription: string;

  seoKeywords: string[];
}

export interface FileInput {
  uid?: Maybe<string>;

  name?: Maybe<string>;

  url: string;
}

export interface ProductMetaInput {
  key: string;

  value: string;
}

export interface DosageInput {
  id?: Maybe<number>;

  dosage: string;

  productId?: Maybe<number>;

  details: DosageDetailInput[];
}

export interface DosageDetailInput {
  id?: Maybe<number>;

  count: number;

  price: number;

  dosageId?: Maybe<number>;
}

export interface ProductUpdateInput {
  id: number;

  title?: Maybe<string>;

  url?: Maybe<string>;

  logo?: Maybe<FileInput>;

  gallery?: Maybe<FileInput[]>;

  withAlcohol?: Maybe<boolean>;

  timeToStart?: Maybe<string>;

  workTime?: Maybe<string>;

  meta?: Maybe<ProductMetaInput[]>;

  dosages?: Maybe<DosageInput[]>;

  description?: Maybe<string>;

  seoTitle?: Maybe<string>;

  seoDescription?: Maybe<string>;

  seoKeywords?: Maybe<string[]>;
}
/** Order delivery types */
export enum DeliveryTypes {
  Courier = "courier",
  Mail = "mail",
  Pickup = "pickup"
}
/** Order delivery status */
export enum DeliveryStatus {
  Pending = "pending",
  InProgress = "inProgress",
  Confirm = "confirm",
  Shipped = "shipped",
  Done = "done"
}
/** Order payment types */
export enum PaymentTypes {
  Cash = "cash",
  Card = "card"
}
/** Order payment status */
export enum PaymentStatus {
  Pending = "pending",
  Paid = "paid",
  Cancel = "cancel"
}

/** The `Upload` scalar type represents a file upload. */
export type Upload = any;

// ====================================================
// Documents
// ====================================================

export type SignInVariables = {
  login: string;
  password: string;
};

export type SignInMutation = {
  __typename?: "Mutation";

  signIn: Maybe<SignInSignIn>;
};

export type SignInSignIn = {
  __typename?: "Token";

  token: string;
};

export type AuthUserVariables = {};

export type AuthUserQuery = {
  __typename?: "Query";

  authUser: Maybe<AuthUserAuthUser>;
};

export type AuthUserAuthUser = {
  __typename?: "UserEntity";

  id: number;

  login: string;
};

export type UploadVariables = {
  file: Upload;
};

export type UploadMutation = {
  __typename?: "Mutation";

  upload: string;
};

export type SearchOrdersVariables = {
  where?: Maybe<OrdersWhereInput>;
  skip?: Maybe<number>;
  take?: Maybe<number>;
};

export type SearchOrdersQuery = {
  __typename?: "Query";

  searchOrders: SearchOrdersSearchOrders;
};

export type SearchOrdersSearchOrders = {
  __typename?: "OrdersPayload";

  count: number;

  data: SearchOrdersData[];
};

export type SearchOrdersData = {
  __typename?: "OrderEntity";

  id: number;

  number: string;

  deliveryType: DeliveryTypes;

  deliveryStatus: DeliveryStatus;

  paymentType: PaymentTypes;

  paymentStatus: PaymentStatus;

  customerName: string;

  phone: Maybe<string>;

  comment: Maybe<string>;

  email: Maybe<string>;

  createdAt: string;

  updatedAt: string;

  address: SearchOrdersAddress;

  cart: SearchOrdersCart[];
};

export type SearchOrdersAddress = {
  __typename?: "OrderAddress";

  city: string;

  street: string;

  apartment: string;

  postalCode: string;
};

export type SearchOrdersCart = {
  __typename?: "CartEntity";

  id: number;

  productId: number;

  dosageId: number;

  dosageDetailId: number;

  quantity: number;

  product: SearchOrdersProduct;

  dosage: SearchOrdersDosage;

  dosageDetail: SearchOrdersDosageDetail;
};

export type SearchOrdersProduct = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  description: string;

  logo: SearchOrdersLogo;

  dosages: SearchOrdersDosages[];
};

export type SearchOrdersLogo = {
  __typename?: "File";

  url: string;
};

export type SearchOrdersDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: SearchOrdersDetails[];
};

export type SearchOrdersDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type SearchOrdersDosage = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;
};

export type SearchOrdersDosageDetail = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type CreateOrderVariables = {
  data: OrderAddInput;
};

export type CreateOrderMutation = {
  __typename?: "Mutation";

  createOrder: CreateOrderCreateOrder;
};

export type CreateOrderCreateOrder = {
  __typename?: "OrderEntity";

  id: number;

  number: string;

  deliveryType: DeliveryTypes;

  deliveryStatus: DeliveryStatus;

  paymentType: PaymentTypes;

  paymentStatus: PaymentStatus;

  customerName: string;

  phone: Maybe<string>;

  comment: Maybe<string>;

  email: Maybe<string>;

  createdAt: string;

  updatedAt: string;

  address: CreateOrderAddress;

  cart: CreateOrderCart[];
};

export type CreateOrderAddress = {
  __typename?: "OrderAddress";

  city: string;

  street: string;

  apartment: string;

  postalCode: string;
};

export type CreateOrderCart = {
  __typename?: "CartEntity";

  id: number;

  productId: number;

  dosageId: number;

  dosageDetailId: number;

  quantity: number;

  product: CreateOrderProduct;

  dosage: CreateOrderDosage;

  dosageDetail: CreateOrderDosageDetail;
};

export type CreateOrderProduct = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  description: string;

  logo: CreateOrderLogo;

  dosages: CreateOrderDosages[];
};

export type CreateOrderLogo = {
  __typename?: "File";

  url: string;
};

export type CreateOrderDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: CreateOrderDetails[];
};

export type CreateOrderDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type CreateOrderDosage = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;
};

export type CreateOrderDosageDetail = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type OrderVariables = {
  where: OrderWhereInput;
};

export type OrderQuery = {
  __typename?: "Query";

  order: Maybe<OrderOrder>;
};

export type OrderOrder = {
  __typename?: "OrderEntity";

  id: number;

  number: string;

  deliveryType: DeliveryTypes;

  deliveryStatus: DeliveryStatus;

  paymentType: PaymentTypes;

  paymentStatus: PaymentStatus;

  customerName: string;

  phone: Maybe<string>;

  comment: Maybe<string>;

  email: Maybe<string>;

  createdAt: string;

  updatedAt: string;

  address: OrderAddress;

  cart: OrderCart[];
};

export type OrderAddress = {
  __typename?: "OrderAddress";

  city: string;

  street: string;

  apartment: string;

  postalCode: string;
};

export type OrderCart = {
  __typename?: "CartEntity";

  id: number;

  productId: number;

  dosageId: number;

  dosageDetailId: number;

  quantity: number;

  product: OrderProduct;

  dosage: OrderDosage;

  dosageDetail: OrderDosageDetail;
};

export type OrderProduct = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  description: string;

  logo: OrderLogo;

  dosages: OrderDosages[];
};

export type OrderLogo = {
  __typename?: "File";

  url: string;
};

export type OrderDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: OrderDetails[];
};

export type OrderDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type OrderDosage = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;
};

export type OrderDosageDetail = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type UpdateOrderVariables = {
  data: OrderUpdateInput;
};

export type UpdateOrderMutation = {
  __typename?: "Mutation";

  updateOrder: UpdateOrderUpdateOrder;
};

export type UpdateOrderUpdateOrder = {
  __typename?: "OrderEntity";

  id: number;

  number: string;

  deliveryType: DeliveryTypes;

  deliveryStatus: DeliveryStatus;

  paymentType: PaymentTypes;

  paymentStatus: PaymentStatus;

  customerName: string;

  phone: Maybe<string>;

  comment: Maybe<string>;

  email: Maybe<string>;

  createdAt: string;

  updatedAt: string;

  address: UpdateOrderAddress;

  cart: UpdateOrderCart[];
};

export type UpdateOrderAddress = {
  __typename?: "OrderAddress";

  city: string;

  street: string;

  apartment: string;

  postalCode: string;
};

export type UpdateOrderCart = {
  __typename?: "CartEntity";

  id: number;

  productId: number;

  dosageId: number;

  dosageDetailId: number;

  quantity: number;

  product: UpdateOrderProduct;

  dosage: UpdateOrderDosage;

  dosageDetail: UpdateOrderDosageDetail;
};

export type UpdateOrderProduct = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  description: string;

  logo: UpdateOrderLogo;

  dosages: UpdateOrderDosages[];
};

export type UpdateOrderLogo = {
  __typename?: "File";

  url: string;
};

export type UpdateOrderDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: UpdateOrderDetails[];
};

export type UpdateOrderDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type UpdateOrderDosage = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;
};

export type UpdateOrderDosageDetail = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type SearchProductsVariables = {
  where?: Maybe<ProductsWhereInput>;
  skip?: Maybe<number>;
  take?: Maybe<number>;
};

export type SearchProductsQuery = {
  __typename?: "Query";

  searchProducts: SearchProductsSearchProducts;
};

export type SearchProductsSearchProducts = {
  __typename?: "ProductsPayload";

  count: number;

  data: SearchProductsData[];
};

export type SearchProductsData = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  logo: SearchProductsLogo;

  dosages: SearchProductsDosages[];

  gallery: Maybe<SearchProductsGallery[]>;

  withAlcohol: boolean;

  timeToStart: string;

  workTime: string;

  meta: Maybe<SearchProductsMeta[]>;

  description: string;

  seoTitle: string;

  seoDescription: string;

  seoKeywords: string[];

  createdAt: string;

  updatedAt: string;
};

export type SearchProductsLogo = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type SearchProductsDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: SearchProductsDetails[];
};

export type SearchProductsDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type SearchProductsGallery = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type SearchProductsMeta = {
  __typename?: "ProductMeta";

  key: string;

  value: string;
};

export type RemoveProductVariables = {
  where: ProductWhereInput;
};

export type RemoveProductMutation = {
  __typename?: "Mutation";

  removeProduct: boolean;
};

export type CreateProductVariables = {
  data: ProductAddInput;
};

export type CreateProductMutation = {
  __typename?: "Mutation";

  createProduct: CreateProductCreateProduct;
};

export type CreateProductCreateProduct = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  logo: CreateProductLogo;

  dosages: CreateProductDosages[];

  gallery: Maybe<CreateProductGallery[]>;

  withAlcohol: boolean;

  timeToStart: string;

  workTime: string;

  meta: Maybe<CreateProductMeta[]>;

  description: string;

  seoTitle: string;

  seoDescription: string;

  seoKeywords: string[];

  createdAt: string;

  updatedAt: string;
};

export type CreateProductLogo = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type CreateProductDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: CreateProductDetails[];
};

export type CreateProductDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type CreateProductGallery = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type CreateProductMeta = {
  __typename?: "ProductMeta";

  key: string;

  value: string;
};

export type ProductVariables = {
  where: ProductWhereInput;
};

export type ProductQuery = {
  __typename?: "Query";

  product: Maybe<ProductProduct>;
};

export type ProductProduct = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  logo: ProductLogo;

  dosages: ProductDosages[];

  gallery: Maybe<ProductGallery[]>;

  withAlcohol: boolean;

  timeToStart: string;

  workTime: string;

  meta: Maybe<ProductMeta[]>;

  description: string;

  seoTitle: string;

  seoDescription: string;

  seoKeywords: string[];

  createdAt: string;

  updatedAt: string;
};

export type ProductLogo = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type ProductDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: ProductDetails[];
};

export type ProductDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type ProductGallery = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type ProductMeta = {
  __typename?: "ProductMeta";

  key: string;

  value: string;
};

export type UpdateProductVariables = {
  data: ProductUpdateInput;
};

export type UpdateProductMutation = {
  __typename?: "Mutation";

  updateProduct: UpdateProductUpdateProduct;
};

export type UpdateProductUpdateProduct = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  logo: UpdateProductLogo;

  dosages: UpdateProductDosages[];

  gallery: Maybe<UpdateProductGallery[]>;

  withAlcohol: boolean;

  timeToStart: string;

  workTime: string;

  meta: Maybe<UpdateProductMeta[]>;

  description: string;

  seoTitle: string;

  seoDescription: string;

  seoKeywords: string[];

  createdAt: string;

  updatedAt: string;
};

export type UpdateProductLogo = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type UpdateProductDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: UpdateProductDetails[];
};

export type UpdateProductDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type UpdateProductGallery = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type UpdateProductMeta = {
  __typename?: "ProductMeta";

  key: string;

  value: string;
};

import gql from "graphql-tag";
import * as React from "react";
import * as ReactApollo from "react-apollo";
import * as ReactApolloHooks from "react-apollo-hooks";

// ====================================================
// Components
// ====================================================

export const SignInDocument = gql`
  mutation SignIn($login: String!, $password: String!) {
    signIn(password: $password, login: $login) {
      token
    }
  }
`;
export class SignInComponent extends React.Component<
  Partial<ReactApollo.MutationProps<SignInMutation, SignInVariables>>
> {
  render() {
    return (
      <ReactApollo.Mutation<SignInMutation, SignInVariables>
        mutation={SignInDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type SignInProps<TChildProps = any> = Partial<
  ReactApollo.MutateProps<SignInMutation, SignInVariables>
> &
  TChildProps;
export type SignInMutationFn = ReactApollo.MutationFn<
  SignInMutation,
  SignInVariables
>;
export function SignInHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        SignInMutation,
        SignInVariables,
        SignInProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    SignInMutation,
    SignInVariables,
    SignInProps<TChildProps>
  >(SignInDocument, operationOptions);
}
export function useSignIn(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    SignInMutation,
    SignInVariables
  >
) {
  return ReactApolloHooks.useMutation<SignInMutation, SignInVariables>(
    SignInDocument,
    baseOptions
  );
}
export const AuthUserDocument = gql`
  query AuthUser {
    authUser {
      id
      login
    }
  }
`;
export class AuthUserComponent extends React.Component<
  Partial<ReactApollo.QueryProps<AuthUserQuery, AuthUserVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<AuthUserQuery, AuthUserVariables>
        query={AuthUserDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type AuthUserProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<AuthUserQuery, AuthUserVariables>
> &
  TChildProps;
export function AuthUserHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        AuthUserQuery,
        AuthUserVariables,
        AuthUserProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    AuthUserQuery,
    AuthUserVariables,
    AuthUserProps<TChildProps>
  >(AuthUserDocument, operationOptions);
}
export function useAuthUser(
  baseOptions?: ReactApolloHooks.QueryHookOptions<AuthUserVariables>
) {
  return ReactApolloHooks.useQuery<AuthUserQuery, AuthUserVariables>(
    AuthUserDocument,
    baseOptions
  );
}
export const UploadDocument = gql`
  mutation Upload($file: Upload!) {
    upload(file: $file)
  }
`;
export class UploadComponent extends React.Component<
  Partial<ReactApollo.MutationProps<UploadMutation, UploadVariables>>
> {
  render() {
    return (
      <ReactApollo.Mutation<UploadMutation, UploadVariables>
        mutation={UploadDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type UploadProps<TChildProps = any> = Partial<
  ReactApollo.MutateProps<UploadMutation, UploadVariables>
> &
  TChildProps;
export type UploadMutationFn = ReactApollo.MutationFn<
  UploadMutation,
  UploadVariables
>;
export function UploadHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        UploadMutation,
        UploadVariables,
        UploadProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    UploadMutation,
    UploadVariables,
    UploadProps<TChildProps>
  >(UploadDocument, operationOptions);
}
export function useUpload(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UploadMutation,
    UploadVariables
  >
) {
  return ReactApolloHooks.useMutation<UploadMutation, UploadVariables>(
    UploadDocument,
    baseOptions
  );
}
export const SearchOrdersDocument = gql`
  query SearchOrders($where: OrdersWhereInput, $skip: Float, $take: Float) {
    searchOrders(where: $where, skip: $skip, take: $take) {
      count
      data {
        id
        number
        deliveryType
        deliveryStatus
        paymentType
        paymentStatus
        customerName
        phone
        comment
        email
        createdAt
        updatedAt
        address {
          city
          street
          apartment
          postalCode
        }
        cart {
          id
          productId
          dosageId
          dosageDetailId
          quantity
          product {
            id
            title
            url
            description
            logo {
              url
            }
            dosages {
              id
              dosage
              details {
                id
                count
                price
              }
            }
          }
          dosage {
            id
            dosage
          }
          dosageDetail {
            id
            count
            price
          }
        }
      }
    }
  }
`;
export class SearchOrdersComponent extends React.Component<
  Partial<ReactApollo.QueryProps<SearchOrdersQuery, SearchOrdersVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<SearchOrdersQuery, SearchOrdersVariables>
        query={SearchOrdersDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type SearchOrdersProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<SearchOrdersQuery, SearchOrdersVariables>
> &
  TChildProps;
export function SearchOrdersHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        SearchOrdersQuery,
        SearchOrdersVariables,
        SearchOrdersProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    SearchOrdersQuery,
    SearchOrdersVariables,
    SearchOrdersProps<TChildProps>
  >(SearchOrdersDocument, operationOptions);
}
export function useSearchOrders(
  baseOptions?: ReactApolloHooks.QueryHookOptions<SearchOrdersVariables>
) {
  return ReactApolloHooks.useQuery<SearchOrdersQuery, SearchOrdersVariables>(
    SearchOrdersDocument,
    baseOptions
  );
}
export const CreateOrderDocument = gql`
  mutation CreateOrder($data: OrderAddInput!) {
    createOrder(data: $data) {
      id
      number
      deliveryType
      deliveryStatus
      paymentType
      paymentStatus
      customerName
      phone
      comment
      email
      createdAt
      updatedAt
      address {
        city
        street
        apartment
        postalCode
      }
      cart {
        id
        productId
        dosageId
        dosageDetailId
        quantity
        product {
          id
          title
          url
          description
          logo {
            url
          }
          dosages {
            id
            dosage
            details {
              id
              count
              price
            }
          }
        }
        dosage {
          id
          dosage
        }
        dosageDetail {
          id
          count
          price
        }
      }
    }
  }
`;
export class CreateOrderComponent extends React.Component<
  Partial<ReactApollo.MutationProps<CreateOrderMutation, CreateOrderVariables>>
> {
  render() {
    return (
      <ReactApollo.Mutation<CreateOrderMutation, CreateOrderVariables>
        mutation={CreateOrderDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type CreateOrderProps<TChildProps = any> = Partial<
  ReactApollo.MutateProps<CreateOrderMutation, CreateOrderVariables>
> &
  TChildProps;
export type CreateOrderMutationFn = ReactApollo.MutationFn<
  CreateOrderMutation,
  CreateOrderVariables
>;
export function CreateOrderHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        CreateOrderMutation,
        CreateOrderVariables,
        CreateOrderProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    CreateOrderMutation,
    CreateOrderVariables,
    CreateOrderProps<TChildProps>
  >(CreateOrderDocument, operationOptions);
}
export function useCreateOrder(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateOrderMutation,
    CreateOrderVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateOrderMutation,
    CreateOrderVariables
  >(CreateOrderDocument, baseOptions);
}
export const OrderDocument = gql`
  query Order($where: OrderWhereInput!) {
    order(where: $where) {
      id
      number
      deliveryType
      deliveryStatus
      paymentType
      paymentStatus
      customerName
      phone
      comment
      email
      createdAt
      updatedAt
      address {
        city
        street
        apartment
        postalCode
      }
      cart {
        id
        productId
        dosageId
        dosageDetailId
        quantity
        product {
          id
          title
          url
          description
          logo {
            url
          }
          dosages {
            id
            dosage
            details {
              id
              count
              price
            }
          }
        }
        dosage {
          id
          dosage
        }
        dosageDetail {
          id
          count
          price
        }
      }
    }
  }
`;
export class OrderComponent extends React.Component<
  Partial<ReactApollo.QueryProps<OrderQuery, OrderVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<OrderQuery, OrderVariables>
        query={OrderDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type OrderProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<OrderQuery, OrderVariables>
> &
  TChildProps;
export function OrderHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        OrderQuery,
        OrderVariables,
        OrderProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    OrderQuery,
    OrderVariables,
    OrderProps<TChildProps>
  >(OrderDocument, operationOptions);
}
export function useOrder(
  baseOptions?: ReactApolloHooks.QueryHookOptions<OrderVariables>
) {
  return ReactApolloHooks.useQuery<OrderQuery, OrderVariables>(
    OrderDocument,
    baseOptions
  );
}
export const UpdateOrderDocument = gql`
  mutation UpdateOrder($data: OrderUpdateInput!) {
    updateOrder(data: $data) {
      id
      number
      deliveryType
      deliveryStatus
      paymentType
      paymentStatus
      customerName
      phone
      comment
      email
      createdAt
      updatedAt
      address {
        city
        street
        apartment
        postalCode
      }
      cart {
        id
        productId
        dosageId
        dosageDetailId
        quantity
        product {
          id
          title
          url
          description
          logo {
            url
          }
          dosages {
            id
            dosage
            details {
              id
              count
              price
            }
          }
        }
        dosage {
          id
          dosage
        }
        dosageDetail {
          id
          count
          price
        }
      }
    }
  }
`;
export class UpdateOrderComponent extends React.Component<
  Partial<ReactApollo.MutationProps<UpdateOrderMutation, UpdateOrderVariables>>
> {
  render() {
    return (
      <ReactApollo.Mutation<UpdateOrderMutation, UpdateOrderVariables>
        mutation={UpdateOrderDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type UpdateOrderProps<TChildProps = any> = Partial<
  ReactApollo.MutateProps<UpdateOrderMutation, UpdateOrderVariables>
> &
  TChildProps;
export type UpdateOrderMutationFn = ReactApollo.MutationFn<
  UpdateOrderMutation,
  UpdateOrderVariables
>;
export function UpdateOrderHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        UpdateOrderMutation,
        UpdateOrderVariables,
        UpdateOrderProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    UpdateOrderMutation,
    UpdateOrderVariables,
    UpdateOrderProps<TChildProps>
  >(UpdateOrderDocument, operationOptions);
}
export function useUpdateOrder(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UpdateOrderMutation,
    UpdateOrderVariables
  >
) {
  return ReactApolloHooks.useMutation<
    UpdateOrderMutation,
    UpdateOrderVariables
  >(UpdateOrderDocument, baseOptions);
}
export const SearchProductsDocument = gql`
  query SearchProducts($where: ProductsWhereInput, $skip: Float, $take: Float) {
    searchProducts(where: $where, skip: $skip, take: $take) {
      count
      data {
        id
        title
        url
        logo {
          uid
          name
          url
        }
        dosages {
          id
          dosage
          details {
            id
            count
            price
          }
        }
        gallery {
          uid
          name
          url
        }
        withAlcohol
        timeToStart
        workTime
        meta {
          key
          value
        }
        description
        seoTitle
        seoDescription
        seoKeywords
        createdAt
        updatedAt
      }
    }
  }
`;
export class SearchProductsComponent extends React.Component<
  Partial<ReactApollo.QueryProps<SearchProductsQuery, SearchProductsVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<SearchProductsQuery, SearchProductsVariables>
        query={SearchProductsDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type SearchProductsProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<SearchProductsQuery, SearchProductsVariables>
> &
  TChildProps;
export function SearchProductsHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        SearchProductsQuery,
        SearchProductsVariables,
        SearchProductsProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    SearchProductsQuery,
    SearchProductsVariables,
    SearchProductsProps<TChildProps>
  >(SearchProductsDocument, operationOptions);
}
export function useSearchProducts(
  baseOptions?: ReactApolloHooks.QueryHookOptions<SearchProductsVariables>
) {
  return ReactApolloHooks.useQuery<
    SearchProductsQuery,
    SearchProductsVariables
  >(SearchProductsDocument, baseOptions);
}
export const RemoveProductDocument = gql`
  mutation RemoveProduct($where: ProductWhereInput!) {
    removeProduct(where: $where)
  }
`;
export class RemoveProductComponent extends React.Component<
  Partial<
    ReactApollo.MutationProps<RemoveProductMutation, RemoveProductVariables>
  >
> {
  render() {
    return (
      <ReactApollo.Mutation<RemoveProductMutation, RemoveProductVariables>
        mutation={RemoveProductDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type RemoveProductProps<TChildProps = any> = Partial<
  ReactApollo.MutateProps<RemoveProductMutation, RemoveProductVariables>
> &
  TChildProps;
export type RemoveProductMutationFn = ReactApollo.MutationFn<
  RemoveProductMutation,
  RemoveProductVariables
>;
export function RemoveProductHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        RemoveProductMutation,
        RemoveProductVariables,
        RemoveProductProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    RemoveProductMutation,
    RemoveProductVariables,
    RemoveProductProps<TChildProps>
  >(RemoveProductDocument, operationOptions);
}
export function useRemoveProduct(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    RemoveProductMutation,
    RemoveProductVariables
  >
) {
  return ReactApolloHooks.useMutation<
    RemoveProductMutation,
    RemoveProductVariables
  >(RemoveProductDocument, baseOptions);
}
export const CreateProductDocument = gql`
  mutation CreateProduct($data: ProductAddInput!) {
    createProduct(data: $data) {
      id
      title
      url
      logo {
        uid
        name
        url
      }
      dosages {
        id
        dosage
        details {
          id
          count
          price
        }
      }
      gallery {
        uid
        name
        url
      }
      withAlcohol
      timeToStart
      workTime
      meta {
        key
        value
      }
      description
      seoTitle
      seoDescription
      seoKeywords
      createdAt
      updatedAt
    }
  }
`;
export class CreateProductComponent extends React.Component<
  Partial<
    ReactApollo.MutationProps<CreateProductMutation, CreateProductVariables>
  >
> {
  render() {
    return (
      <ReactApollo.Mutation<CreateProductMutation, CreateProductVariables>
        mutation={CreateProductDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type CreateProductProps<TChildProps = any> = Partial<
  ReactApollo.MutateProps<CreateProductMutation, CreateProductVariables>
> &
  TChildProps;
export type CreateProductMutationFn = ReactApollo.MutationFn<
  CreateProductMutation,
  CreateProductVariables
>;
export function CreateProductHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        CreateProductMutation,
        CreateProductVariables,
        CreateProductProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    CreateProductMutation,
    CreateProductVariables,
    CreateProductProps<TChildProps>
  >(CreateProductDocument, operationOptions);
}
export function useCreateProduct(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateProductMutation,
    CreateProductVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateProductMutation,
    CreateProductVariables
  >(CreateProductDocument, baseOptions);
}
export const ProductDocument = gql`
  query Product($where: ProductWhereInput!) {
    product(where: $where) {
      id
      title
      url
      logo {
        uid
        name
        url
      }
      dosages {
        id
        dosage
        details {
          id
          count
          price
        }
      }
      gallery {
        uid
        name
        url
      }
      withAlcohol
      timeToStart
      workTime
      meta {
        key
        value
      }
      description
      seoTitle
      seoDescription
      seoKeywords
      createdAt
      updatedAt
    }
  }
`;
export class ProductComponent extends React.Component<
  Partial<ReactApollo.QueryProps<ProductQuery, ProductVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<ProductQuery, ProductVariables>
        query={ProductDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type ProductProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<ProductQuery, ProductVariables>
> &
  TChildProps;
export function ProductHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        ProductQuery,
        ProductVariables,
        ProductProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    ProductQuery,
    ProductVariables,
    ProductProps<TChildProps>
  >(ProductDocument, operationOptions);
}
export function useProduct(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ProductVariables>
) {
  return ReactApolloHooks.useQuery<ProductQuery, ProductVariables>(
    ProductDocument,
    baseOptions
  );
}
export const UpdateProductDocument = gql`
  mutation UpdateProduct($data: ProductUpdateInput!) {
    updateProduct(data: $data) {
      id
      title
      url
      logo {
        uid
        name
        url
      }
      dosages {
        id
        dosage
        details {
          id
          count
          price
        }
      }
      gallery {
        uid
        name
        url
      }
      withAlcohol
      timeToStart
      workTime
      meta {
        key
        value
      }
      description
      seoTitle
      seoDescription
      seoKeywords
      createdAt
      updatedAt
    }
  }
`;
export class UpdateProductComponent extends React.Component<
  Partial<
    ReactApollo.MutationProps<UpdateProductMutation, UpdateProductVariables>
  >
> {
  render() {
    return (
      <ReactApollo.Mutation<UpdateProductMutation, UpdateProductVariables>
        mutation={UpdateProductDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type UpdateProductProps<TChildProps = any> = Partial<
  ReactApollo.MutateProps<UpdateProductMutation, UpdateProductVariables>
> &
  TChildProps;
export type UpdateProductMutationFn = ReactApollo.MutationFn<
  UpdateProductMutation,
  UpdateProductVariables
>;
export function UpdateProductHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        UpdateProductMutation,
        UpdateProductVariables,
        UpdateProductProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    UpdateProductMutation,
    UpdateProductVariables,
    UpdateProductProps<TChildProps>
  >(UpdateProductDocument, operationOptions);
}
export function useUpdateProduct(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UpdateProductMutation,
    UpdateProductVariables
  >
) {
  return ReactApolloHooks.useMutation<
    UpdateProductMutation,
    UpdateProductVariables
  >(UpdateProductDocument, baseOptions);
}
