import React, { useEffect } from "react";
import { withRoute } from "react-router5";
import { RouteContext } from "react-router5/types/types";
import { compose } from "recompose";
import RootRouter from "../root-router";
import { AuthUserAuthUser, useAuthUser } from "./apollo-components";
import { Spinner } from "./spinner";

type Props = RouteContext & {
  authUser?: AuthUserAuthUser;
};

const enhance = compose<any, any>(withRoute);

const App = ({ router, route }: Props) => {
  const { data, loading } = useAuthUser();

  useEffect(() => {
    if (!loading && !data!.authUser && route.name !== "signIn") {
      router.navigate("signIn");
    }

    if (!loading && data!.authUser && route.name === "signIn") {
      router.navigate("home");
    }
  }, [data!.authUser]);

  if (loading) {
    return <Spinner />;
  }

  return <RootRouter />;
};

export default enhance(App);
