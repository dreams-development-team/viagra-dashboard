import { Checkbox } from "antd";
import { CheckboxProps } from "antd/lib/checkbox";
import { Field, FieldProps } from "formik";
import React from "react";
import FormItem, { FormItemProps } from "./form-item";

const CheckboxComponent = ({
  field,
  form: { touched, errors },
  wrapperCol,
  labelCol,
  label,
  help,
  required,
  ...props
}: FieldProps & FormItemProps) => (
  <FormItem
    required={required}
    label={label}
    labelCol={labelCol}
    wrapperCol={wrapperCol}
    validateStatus={touched[field.name] && errors[field.name] ? "error" : ""}
    help={(touched[field.name] && errors[field.name]) || help}
  >
    <Checkbox {...field} {...props} checked={Boolean(field.value)} />
  </FormItem>
);

type Props = CheckboxProps &
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > &
  FormItemProps;

const CheckboxField = (props: Props) => (
  <Field {...props} component={CheckboxComponent} />
);

export default CheckboxField;
