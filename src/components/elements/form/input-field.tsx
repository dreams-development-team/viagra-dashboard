import { Input } from "antd";
import { InputProps } from "antd/lib/input";
import { Field, FieldProps } from "formik";
import React from "react";
import FormItem, { FormItemProps } from "./form-item";

const InputComponent = ({
  field,
  form: { touched, errors },
  wrapperCol,
  labelCol,
  label,
  help,
  required,
  ...props
}: FieldProps & FormItemProps) => (
  <FormItem
    required={required}
    label={label}
    labelCol={labelCol}
    wrapperCol={wrapperCol}
    validateStatus={touched[field.name] && errors[field.name] ? "error" : ""}
    help={(touched[field.name] && errors[field.name]) || help}
  >
    <Input type="text" {...field} {...props} />
  </FormItem>
);

type Props = InputProps &
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > &
  FormItemProps;

const InputField = (props: Props) => (
  <Field {...props} component={InputComponent} />
);

export default InputField;
