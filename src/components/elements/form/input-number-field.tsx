import { InputNumber } from "antd";
import { InputNumberProps } from "antd/lib/input-number";
import { Field, FieldProps } from "formik";
import React from "react";
import FormItem, { FormItemProps } from "./form-item";

const InputNumberComponent = ({
  field,
  form: { touched, errors },
  wrapperCol,
  labelCol,
  label,
  help,
  required,
  fullWidth,
  style,
  ...props
}: FieldProps & FormItemProps & { fullWidth?: boolean }) => {
  const _style = { ...style } as React.CSSProperties;

  if (fullWidth) {
    _style.width = "100%";
  }

  return (
    <FormItem
      required={required}
      label={label}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      validateStatus={touched[field.name] && errors[field.name] ? "error" : ""}
      help={(touched[field.name] && errors[field.name]) || help}
    >
      <InputNumber type="text" {...field} {...props} style={_style} />
    </FormItem>
  );
};

type Props = InputNumberProps &
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > & { fullWidth?: boolean } & FormItemProps;

const InputNumberField = (props: Props) => (
  <Field {...props} component={InputNumberComponent} />
);

export default InputNumberField;
