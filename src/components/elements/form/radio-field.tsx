import { Radio } from "antd";
import { RadioProps } from "antd/lib/radio";
import RadioButton from "antd/lib/radio/radioButton";
import { Field, FieldProps } from "formik";
import React from "react";
import FormItem, { FormItemProps } from "./form-item";

const { Group } = Radio;

const RadioGroupComponent = ({
  field,
  form: { touched, errors },
  wrapperCol,
  labelCol,
  label,
  help,
  required,
  children,
  ...props
}: FieldProps & FormItemProps & { children: React.ReactNode }) => (
  <FormItem
    required={required}
    label={label}
    labelCol={labelCol}
    wrapperCol={wrapperCol}
    validateStatus={touched[field.name] && errors[field.name] ? "error" : ""}
    help={(touched[field.name] && errors[field.name]) || help}
  >
    <Group {...field} {...props}>
      {children}
    </Group>
  </FormItem>
);

type Props = RadioProps &
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > &
  FormItemProps;

const RadioGroupField = (props: Props) => (
  <Field {...props} component={RadioGroupComponent} />
);

RadioGroupField.Button = RadioButton;
RadioGroupField.Radio = Radio;

export default RadioGroupField;
