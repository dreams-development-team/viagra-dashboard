import { Select } from "antd";
import { SelectProps } from "antd/lib/select";
import { Field } from "formik";
import React from "react";
import FormItem, { FormItemProps } from "./form-item";

const SelectComponent = ({
  field,
  wrapperCol,
  labelCol,
  label,
  help,
  required,
  form: { touched, errors, setFieldValue },
  ...props
}) => {
  return (
    <FormItem
      required={required}
      label={label}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      validateStatus={touched[field.name] && errors[field.name] ? "error" : ""}
      help={(touched[field.name] && errors[field.name]) || help}
    >
      <Select
        {...field}
        {...props}
        onChange={value => setFieldValue(field.name, value)}
        onBlur={value => setFieldValue(field.name, value)}
      />
      {touched[field.name] && errors[field.name] && (
        <div className="error">{errors[field.name]}</div>
      )}
    </FormItem>
  );
};

type Props = SelectProps &
  FormItemProps &
  React.DetailedHTMLProps<
    React.SelectHTMLAttributes<HTMLSelectElement>,
    HTMLSelectElement
  >;

const SelectField = (props: Props) => (
  <Field {...props} component={SelectComponent} />
);

export default SelectField;
