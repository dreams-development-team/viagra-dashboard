import { Input } from "antd";
import { TextAreaProps } from "antd/lib/input";
import { Field, FieldProps } from "formik";
import React from "react";
import FormItem, { FormItemProps } from "./form-item";

const { TextArea } = Input;

const TextComponent = ({
  field,
  form: { touched, errors },
  wrapperCol,
  labelCol,
  label,
  help,
  required,
  ...props
}: FieldProps & FormItemProps) => (
  <FormItem
    required={required}
    label={label}
    labelCol={labelCol}
    wrapperCol={wrapperCol}
    validateStatus={touched[field.name] && errors[field.name] ? "error" : ""}
    help={(touched[field.name] && errors[field.name]) || help}
  >
    <TextArea {...field} {...props} />
  </FormItem>
);

type Props = TextAreaProps &
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLTextAreaElement>,
    HTMLTextAreaElement
  > &
  FormItemProps;

const TextField = (props: Props) => (
  <Field {...props} component={TextComponent} />
);

export default TextField;
