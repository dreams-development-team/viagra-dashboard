import { Dropdown, Icon, Menu } from "antd";
import { isNil } from "lodash";
import React, { useState } from "react";
import { compose } from "react-apollo";
import { useApolloClient } from "react-apollo-hooks";
import { Link, withRoute } from "react-router5";
import { RouteContext } from "react-router5/types/types";
import store from "store";
import { useAuthUser } from "./apollo-components";

type Props = RouteContext & {
  children: JSX.Element;
};

const Item = Menu.Item;

const menu = ({ handleLogout }) => (
  <Menu>
    <Menu.Item onClick={handleLogout}>Logout</Menu.Item>
  </Menu>
);

const enhance = compose(withRoute);

const HomeLayout = ({ children, route }: Props) => {
  const { data } = useAuthUser();
  const { menuKey, handleMenuClick } = useMenuState(
    getMenuKeyFromPath(route.path)
  );
  const client = useApolloClient();

  const handleLogout = async () => {
    store.remove("token");
    await client.resetStore();
  };

  if (isNil(data!.authUser)) {
    return null;
  }

  return (
    <section
      style={{
        margin: "0 auto",
        maxWidth: "70vw",
        height: "100%",
        display: "flex",
        flexDirection: "column"
      }}
    >
      <header>
        <div style={{ display: "flex", padding: "30px 0" }}>
          <h1 style={{ fontSize: "2em", flexGrow: 1 }}>Dashboard</h1>
          <Dropdown.Button overlay={menu({ handleLogout })}>
            <span>{data!.authUser!.login}</span>
          </Dropdown.Button>
        </div>
      </header>
      <Menu
        onClick={handleMenuClick}
        selectedKeys={[menuKey]}
        mode="horizontal"
        style={{ marginBottom: 40 }}
      >
        <Item key="home">
          <Icon type="home" />
          <Link routeName="home" style={{ display: "inline-block" }}>
            Home
          </Link>
        </Item>
        <Item key="products">
          <Icon type="shop" />
          <Link routeName="home.products" style={{ display: "inline-block" }}>
            Products
          </Link>
        </Item>
        <Item key="orders">
          <Icon type="rocket" />
          <Link routeName="home.orders" style={{ display: "inline-block" }}>
            Orders
          </Link>
        </Item>
      </Menu>
      <main style={{ marginBottom: 20, flexGrow: 1 }}>{children}</main>
      <footer style={{ textAlign: "center", padding: "30px 0px" }}>
        Shop ©2019 Created by{" "}
        <a target="_blank" href="https://github.com/mrkode">
          khotey
        </a>
      </footer>
    </section>
  );
};

function useMenuState(defaultKey: string) {
  const [key, setKey] = useState(defaultKey);

  return {
    menuKey: key,
    handleMenuClick({ key: menuKey }) {
      setKey(menuKey);
    }
  };
}

function getMenuKeyFromPath(path: string, defaultKey: string = "") {
  if (path === "/") {
    return "home";
  }

  let key = defaultKey;
  const match = path.match(/\/([a-z0-9-]+)(\/\S+)?/);

  if (match && match[1]) {
    key = match[1];
  }

  return key;
}

export default enhance(HomeLayout);
