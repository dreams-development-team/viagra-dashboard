import { Spin } from "antd";
import React from "react";
import { Center } from "./helpers";

export const Spinner = () => {
  return (
    <Center>
      <Spin />
    </Center>
  );
};
