import { defaultTo, isEmpty, lowerCase, upperFirst } from "lodash";

export const ANT_SORT: any = { descend: "DESC", ascend: "ASC" };
export const NORMAL_SORT: any = { DESC: "descend", ASC: "ascend" };

export function encodeSort(sort: string) {
  return NORMAL_SORT[sort];
}

export function decodeSort(sort: string) {
  return ANT_SORT[sort];
}

export function getCurrentPage(offset: number, limit: number) {
  return defaultTo(offset, 0) / limit + 1;
}

export function getOffset(currentPage: number = 1, limit: number = 0) {
  return limit * (currentPage - 1);
}

export function getSort(tableSort) {
  if (isEmpty(tableSort)) {
    return {};
  } else {
    return parseOrderFromTable(tableSort);
  }
}

export function parseOrderFromTable({
  field,
  order
}: {
  field: string;
  order: string;
}) {
  return { [`order${upperFirst(field)}`]: decodeSort(order) || undefined };
}

export function parseOrderFromQuery({
  field,
  order
}: {
  field: string;
  order: string;
}) {
  return { [`order${lowerCase(field)}`]: encodeSort(order) || undefined };
}
