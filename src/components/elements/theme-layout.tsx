import { Global } from "@emotion/core";
import { ThemeProvider as Provider } from "emotion-theming";
import React, { ReactNode } from "react";
import { globalStyles, theme } from "src/theme";

const ThemeLayout = ({ children }: { children: ReactNode }) => {
  return (
    <>
      <Global styles={globalStyles} />
      <Provider theme={theme}>{children}</Provider>
    </>
  );
};

export default ThemeLayout;
