import { Icon, Modal, Upload } from "antd";
import { UploadProps } from "antd/lib/upload";
import React, { useState } from "react";
import { FileDto } from "src/dto";

type Props = UploadProps & {
  onUpload?: (file: FileDto) => void;
  onRemove?: (file: FileDto) => any | Promise<any> | void | Promise<void>;
  files?: FileDto[];
  file?: FileDto;
};

const Uploader = ({
  files,
  file,
  multiple,
  listType,
  onRemove,
  onUpload
}: Props) => {
  const { state, handlePreview, handleCancel } = usePreviewState(listType);

  return (
    <div className="clearfix">
      <Upload
        multiple={multiple}
        listType={listType}
        fileList={files as any}
        action={onUpload as any}
        onPreview={handlePreview}
        onRemove={onRemove}
        showUploadList={multiple}
        customRequest={({ _, onSuccess }) => {
          setTimeout(() => {
            onSuccess("ok");
          }, 0);
        }}
      >
        {file && !multiple && file.status !== "uploading" ? (
          <Image file={file} onRemove={onRemove} />
        ) : (
          <UploadButton loading={file && file.status === "uploading"} />
        )}
      </Upload>
      <Modal
        visible={state.previewVisible}
        footer={null}
        onCancel={handleCancel}
      >
        <img alt="example" style={{ width: "100%" }} src={state.previewImage} />
      </Modal>
    </div>
  );
};

function UploadButton({ loading }: { loading?: boolean }) {
  return (
    <div>
      <Icon type={loading ? "loading" : "plus"} />
      <div className="ant-upload-text">Upload</div>
    </div>
  );
}

function Image({ file, onRemove }) {
  return (
    <div style={{ position: "relative" }}>
      <Icon
        type="delete"
        theme="twoTone"
        onClick={() => onRemove(file)}
        style={{
          position: "absolute",
          zIndex: 9999,
          fontSize: 20,
          top: 15,
          left: 15
        }}
      />
      <img
        src={file.url}
        alt="avatar"
        style={{ maxWidth: 200, height: "auto" }}
      />
    </div>
  );
}

const initPreviewState = {
  previewImage: "",
  previewVisible: false
};

function usePreviewState(type) {
  const [state, setPreview] = useState(initPreviewState);

  const handlePreview = file => {
    if (type === "text") {
      return;
    }

    setPreview({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true
    });
  };

  const handleCancel = () => setPreview(initPreviewState);

  return {
    state,
    handleCancel,
    handlePreview
  };
}

export default Uploader;
