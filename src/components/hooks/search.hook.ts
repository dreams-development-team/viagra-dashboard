import { useSetState } from "react-use";

type SearchState = {
  currentPage?: number;
  variables?: { [key: string]: any };
};

const initialState = {
  currentPage: 1,
  variables: {}
};

export const useSearchState = (initState: SearchState = initialState) => {
  const [state, setState] = useSetState(initState);

  const setSearch = (search: SearchState) => {
    setState(search);
  };

  return {
    search: state,
    setSearch
  };
};
