import React from "react";
import { routeNode } from "react-router5";
import { RouteContext } from "react-router5/types/types";
import { compose } from "recompose";
import HomeLayout from "./elements/home-layout";
import { Router as HomeRouter } from "./views/home";
import SignInView from "./views/sign-in";

type Props = RouteContext;

const enhance = compose<Props, any>(routeNode(""));

const RootRouter = ({ route }: Props) => {
  const topRouteName = route ? route.name.split(".")[0] : null;

  if (topRouteName === "home") {
    return (
      <HomeLayout>
        <HomeRouter />
      </HomeLayout>
    );
  }

  if (topRouteName === "signIn") {
    return <SignInView />;
  }

  return <h2>404 Error: Page Not Found</h2>;
};

export default enhance(RootRouter);
