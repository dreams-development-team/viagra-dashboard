import { isArray } from "lodash";
import { FileDto } from "src/dto";
import { Container } from "unstated";

type FileManagerState = {
  [dataKey: string]: FileDto | FileDto[] | undefined;
};

type FilePayload = {
  key: string;
  data: FileDto | FileDto[];
};

export class FileManagerContainer extends Container<FileManagerState> {
  state: FileManagerState = {};

  addFiles(payload: FilePayload) {
    this.setState({ [payload.key]: payload.data }).catch();
  }

  removeFiles(payload: { key: string }) {
    this.setState({ [payload.key]: undefined }).catch();
  }

  addFile(payload: FilePayload) {
    this.setState(prevState => {
      const data = prevState[payload.key];
      let newData;

      if (isArray(data)) {
        newData = [...data, payload.data];
      } else {
        newData = payload.data;
      }

      return {
        [payload.key]: newData
      };
    }).catch();
  }

  removeFile(payload: FilePayload & { match?: (file: FileDto) => boolean }) {
    this.setState(prevState => {
      const data = prevState[payload.key];

      if (isArray(data)) {
        return { [payload.key]: data.filter(payload.match!) };
      } else {
        return { [payload.key]: undefined };
      }
    }).catch();
  }

  updateFile(payload: FilePayload & { match?: (file: FileDto) => boolean }) {
    this.setState(prevState => {
      const data = prevState[payload.key];

      if (isArray(data)) {
        return {
          [payload.key]: data.map(f => {
            if (payload.match!(f)) {
              return payload.data as FileDto;
            }

            return f;
          })
        };
      } else {
        return { [payload.key]: payload.data };
      }
    }).catch();
  }

  upsertFile(payload: FilePayload & { match?: (file: FileDto) => boolean }) {
    this.setState(prevState => {
      const data = prevState[payload.key];

      if (isArray(data)) {
        return {
          [payload.key]: data.map(f => {
            if (payload.match!(f)) {
              return { ...f, ...payload.data };
            }

            return f;
          })
        };
      } else {
        return { [payload.key]: { ...data, ...payload.data } };
      }
    }).catch();
  }
}
