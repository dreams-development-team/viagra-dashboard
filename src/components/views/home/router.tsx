import React from "react";
import { routeNode } from "react-router5";
import { RouteContext } from "react-router5/types/types";
import { compose, setDisplayName } from "recompose";
import { includesSegment } from "router5-helpers";
import { Router as OrdersRouter } from "../orders";
import { Router as ProductsRouter } from "../products";
import View from "./view";

type Props = RouteContext;

const enhance = compose<Props, any>(
  setDisplayName("src/components/views/home/router"),
  routeNode("home")
);

const Router = ({ route }: Props) => {
  if (includesSegment(route, "products")) {
    return <ProductsRouter />;
  }

  if (includesSegment(route, "orders")) {
    return <OrdersRouter />;
  }

  return <View />;
};

export default enhance(Router);
