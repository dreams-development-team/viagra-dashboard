import React from "react";
import { compose, setDisplayName } from "recompose";
import { Title } from "./styles";

const enhance = compose(setDisplayName("src/components/views/home/view"));

const View = () => {
  return <Title>Welcome!</Title>;
};

export default enhance(View);
