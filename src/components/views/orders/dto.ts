// tslint:disable:variable-name
import { Exclude, Expose, plainToClass, Type } from "class-transformer";
import {
  AddressInput,
  CartInput,
  DeliveryStatus,
  DeliveryTypes,
  OrderAddInput,
  OrderDetails,
  OrderDosage,
  OrderOrder,
  PaymentStatus,
  PaymentTypes,
  ProductProduct
} from "src/components/elements/apollo-components";

export class OrderDto implements OrderAddInput {
  public static getInitialValues() {
    return plainToClass(OrderDto, {
      address: {
        city: "",
        street: "",
        apartment: "",
        postalCode: ""
      },
      comment: "",
      phone: "",
      email: "",
      deliveryType: DeliveryTypes.Courier,
      deliveryStatus: DeliveryStatus.Confirm,
      paymentType: PaymentTypes.Cash,
      paymentStatus: PaymentStatus.Pending,
      cart: []
    });
  }

  public static unwrap(value: OrderDto | OrderOrder) {
    return plainToClass(OrderDto, value, {
      groups: ["withCartProduct", "withCartDosage", "withCartDosageDetail"]
    });
  }

  public static wrap(value: OrderDto | OrderOrder) {
    return plainToClass(OrderDto, value);
  }

  id?: number;

  @Type(() => AddressDto)
  address: AddressInput;

  @Type(() => CartDto)
  cart: CartDto[];

  comment?: string;

  email?: string;

  customerName: string;

  deliveryStatus: DeliveryStatus;

  deliveryType: DeliveryTypes;

  paymentStatus: PaymentStatus;

  paymentType: PaymentTypes;

  phone?: string;

  @Exclude()
  __typename?: string;

  @Exclude()
  number?: string;

  @Exclude()
  createdAt: string;

  @Exclude()
  updatedAt: string;
}

export class AddressDto implements AddressInput {
  apartment: string;

  city: string;

  postalCode: string;

  street: string;

  @Exclude()
  __typename?: string;
}

export class CartDto implements CartInput {
  dosageDetailId: number;

  dosageId: number;

  id?: number;

  productId: number;

  @Expose({ groups: ["withCartProduct"] })
  product?: ProductProduct;

  @Expose({ groups: ["withCartDosage"] })
  dosage?: OrderDosage;

  @Expose({ groups: ["withCartDosageDetail"] })
  dosageDetail?: OrderDetails;

  quantity: number;

  @Exclude()
  __typename?: string;
}
