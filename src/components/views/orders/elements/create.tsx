import React, { useCallback, useState } from "react";
import { compose, setDisplayName } from "recompose";
import { useCreateOrder } from "src/components/elements/apollo-components";
import { OrderDto } from "../dto";
import Form from "./form";

type Props = {
  navigate: (routeName: string) => void;
};

const enhance = compose<Props, Props>(
  setDisplayName("src/components/views/orders/elements/create")
);

const Create = ({ navigate }: Props) => {
  const [loading, setLoading] = useState(false);
  const mutation = useCreateOrder();

  const handleSubmit = useCallback(async values => {
    try {
      setLoading(true);
      await mutation({
        variables: { data: values }
      });
      setLoading(false);

      navigate("home.orders");
    } catch {
      setLoading(false);
    }
  }, []);

  return (
    <Form
      loading={loading}
      initialValues={OrderDto.getInitialValues()}
      onSubmit={handleSubmit}
    />
  );
};

export default enhance(Create);
