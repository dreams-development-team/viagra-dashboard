import { Col } from "antd";
import React from "react";
import { InputField } from "src/components/elements/form";

const Address = () => {
  return (
    <>
      <Col xs={24} sm={6}>
        <InputField name="address.city" label="City" required={true} />
      </Col>
      <Col xs={24} sm={6}>
        <InputField name="address.street" label="Street" required={true} />
      </Col>
      <Col xs={24} sm={6}>
        <InputField
          name="address.apartment"
          label="Apartment"
          required={true}
        />
      </Col>
      <Col xs={24} sm={6}>
        <InputField
          name="address.postalCode"
          label="Postal Code"
          required={true}
        />
      </Col>
    </>
  );
};

export default Address;
