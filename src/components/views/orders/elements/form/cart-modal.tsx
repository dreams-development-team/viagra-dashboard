import { Modal } from "antd";
import React, { useCallback, useState } from "react";
import {
  ProductProduct,
  useSearchProducts
} from "src/components/elements/apollo-components";
import Table from "src/components/elements/table";
import { getOffset } from "src/components/elements/table/lib";
import { useSearchState } from "src/components/hooks";
import Search from "../../../products/elements/list/search";
import { getColumns } from "./get-columns";

type Props = {
  visible: boolean;
  onOk: (products: ProductProduct[]) => void;
  onCancel: () => void;
};

const CartModal = ({ visible, onOk, onCancel }: Props) => {
  const { data, loading, refetch } = useSearchProducts({
    variables: {},
    notifyOnNetworkStatusChange: true,
    fetchPolicy: "network-only"
  });
  const { search, setSearch } = useSearchState();
  const [selectedProductsRows, setSelectedProducts] = useState<{
    selectedRows: ProductProduct[];
    selectedRowKeys: number[];
  }>({
    selectedRows: [],
    selectedRowKeys: []
  });

  const handleSearch = useCallback(async values => {
    setSearch({
      currentPage: 1,
      variables: { where: values, skip: 0 }
    });

    await refetch({ where: values, skip: 0 });
  }, []);

  return (
    <Modal
      visible={visible}
      onCancel={() => {
        onCancel();
        setSelectedProducts({
          selectedRowKeys: [],
          selectedRows: []
        });
      }}
      onOk={() => {
        onOk(selectedProductsRows.selectedRows);
        setSelectedProducts({
          selectedRowKeys: [],
          selectedRows: []
        });
      }}
      width="60vw"
      title="Product Search"
    >
      <Search onSubmit={handleSearch} />
      <Table
        loading={loading}
        rowKey="id"
        dataSource={data!.searchProducts && data!.searchProducts.data}
        pagination={{
          total: data!.searchProducts && data!.searchProducts.count,
          current: search.currentPage,
          defaultPageSize: 20,
          style: { marginRight: 20 },
          async onChange(cp, s = 0) {
            setSearch({ currentPage: cp });

            await refetch({ skip: getOffset(cp, s) });
          }
        }}
        rowSelection={{
          selectedRowKeys: selectedProductsRows.selectedRowKeys,
          onChange(selectedRowKeys, selectedRows) {
            setSelectedProducts({
              selectedRowKeys,
              selectedRows
            } as any);
          }
        }}
        columns={getColumns()}
      />
    </Modal>
  );
};

export default CartModal;
