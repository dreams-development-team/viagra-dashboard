import { Checkbox, InputNumber } from "antd";
import React, { useState } from "react";
import {
  CartInput,
  ProductDetails,
  ProductDosages
} from "src/components/elements/apollo-components";
import { FormItem } from "src/components/elements/form";
import { CartDto } from "../../dto";

type Props = {
  cartIndex: number;
  dosage: ProductDosages;
  detail: ProductDetails;
  productId: number;
  cart: CartInput[];
  addToCard: (c: CartDto) => void;
  removeFromCart: (index: number) => void;
  replaceCart: (index: number, value: CartInput) => void;
};

const CartProductData = ({
  cartIndex,
  dosage,
  detail,
  cart,
  productId,
  addToCard,
  removeFromCart,
  replaceCart
}: Props) => {
  const inCart = cartIndex > -1;

  const [quantity, setQuantity] = useState(
    inCart ? cart[cartIndex].quantity : 1
  );

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center"
      }}
    >
      <div>
        <div>
          <b>Count:</b> {detail.count}
        </div>
        <div>
          <b>Price:</b> {detail.price}
        </div>
        <FormItem label="Quantity" required={true}>
          <InputNumber
            min={1}
            value={quantity}
            disabled={!inCart}
            onChange={v => {
              setQuantity(v as number);

              inCart &&
                replaceCart(cartIndex, {
                  productId,
                  dosageDetailId: detail.id,
                  dosageId: dosage.id,
                  quantity: v as number
                });
            }}
          />
        </FormItem>
      </div>
      <Checkbox
        defaultChecked={cartIndex > -1}
        onChange={({ target: { checked } }) => {
          if (checked) {
            addToCard({
              productId,
              dosageDetailId: detail.id,
              dosageId: dosage.id,
              quantity
            });
          } else {
            removeFromCart(cartIndex);
          }
        }}
      />
    </div>
  );
};

export default CartProductData;
