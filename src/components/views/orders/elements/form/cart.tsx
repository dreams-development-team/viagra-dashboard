import { Button, Card, Col, List, Row } from "antd";
import deepEqual from "fast-deep-equal";
import { FieldArray } from "formik";
import numeral from "numeral";
import React, { useCallback, useState } from "react";
import { Link } from "react-router5";
import {
  CartInput,
  ProductProduct
} from "src/components/elements/apollo-components";
import { IconText } from "src/components/elements/helpers";
import CartModal from "./cart-modal";
import CartProductData from "./cart-product-data";

type Props = {
  cart: CartInput[];
  products: { [id: number]: ProductProduct };
  setCartProduct: (patch: { [id: number]: ProductProduct }) => void;
};

const Cart = ({ cart, setCartProduct, products }: Props) => {
  const [visible, setVisible] = useState(false);

  const handleModalOk = useCallback((selectedProducts: ProductProduct[]) => {
    selectedProducts.forEach(p => {
      setCartProduct({ [p.id]: p });
    });

    setVisible(false);
  }, []);

  return (
    <Card type="inner" title="Cart" style={{ marginBottom: 24 }}>
      <CartModal
        visible={visible}
        onOk={handleModalOk}
        onCancel={() => setVisible(false)}
      />
      <Row gutter={30}>
        <Col xs={24} sm={24}>
          <FieldArray
            name="cart"
            render={fieldProps => {
              return (
                <List
                  itemLayout="vertical"
                  size="large"
                  dataSource={Object.values(products)}
                  footer={
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between"
                      }}
                    >
                      <Button
                        htmlType="button"
                        onClick={() => setVisible(true)}
                      >
                        AddProduct
                      </Button>
                      <div>
                        <b>Total Price:</b> {getTotalPrice(cart, products)}
                      </div>
                    </div>
                  }
                  renderItem={product => {
                    return (
                      <List.Item
                        key={product.id}
                        actions={[
                          <IconText
                            key="remove"
                            type="delete"
                            text="remove"
                            onClick={() => {
                              cart.forEach(({ productId }, cIndex) => {
                                if (productId === product.id) {
                                  fieldProps.remove(cIndex);
                                }

                                delete products[product.id];
                              });
                            }}
                          />
                        ]}
                        extra={
                          <img width={272} alt="logo" src={product.logo.url} />
                        }
                      >
                        <List.Item.Meta
                          title={
                            <Link
                              routeName="home.products.update"
                              routeParams={{ id: product.id }}
                              target="_blank"
                            >
                              {product.title}
                            </Link>
                          }
                          description={product.description}
                        />
                        <div>
                          {product.dosages.map(dosage => {
                            return (
                              <Card
                                title={`Dosage: ${dosage.dosage}`}
                                style={{ marginBottom: 24 }}
                                key={dosage.id}
                              >
                                {dosage.details.map(detail => {
                                  return (
                                    <Card.Grid key={detail.id}>
                                      <CartProductData
                                        cartIndex={cart.findIndex(
                                          ({ dosageDetailId, dosageId }) =>
                                            deepEqual(
                                              { dosageId, dosageDetailId },
                                              {
                                                dosageId: dosage.id,
                                                dosageDetailId: detail.id
                                              }
                                            )
                                        )}
                                        cart={cart}
                                        productId={product.id}
                                        detail={detail}
                                        dosage={dosage}
                                        addToCard={fieldProps.push}
                                        removeFromCart={fieldProps.remove}
                                        replaceCart={fieldProps.replace}
                                      />
                                    </Card.Grid>
                                  );
                                })}
                              </Card>
                            );
                          })}
                        </div>
                      </List.Item>
                    );
                  }}
                />
              );
            }}
          />
        </Col>
      </Row>
    </Card>
  );
};

function getTotalPrice(
  cart: CartInput[],
  products: { [id: number]: ProductProduct }
) {
  const total = cart.reduce((acc, c) => {
    return (
      (acc +
        products[c.productId].dosages
          .find(({ id }) => c.dosageId === id)!
          .details.find(({ id }) => c.dosageDetailId === id)!.price) *
      c.quantity
    );
  }, 0);

  return numeral(total).format("$0,0.00");
}

export default Cart;
