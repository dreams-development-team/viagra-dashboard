import { Col, Select } from "antd";
import React from "react";
import {
  DeliveryStatus,
  DeliveryTypes,
  PaymentStatus,
  PaymentTypes
} from "src/components/elements/apollo-components";
import { SelectField } from "src/components/elements/form";

const Details = () => {
  return (
    <>
      <Col xs={24} sm={6}>
        <SelectField name="deliveryType" label="Delivery Type" required={true}>
          {Object.values(DeliveryTypes).map(dt => (
            <Select.Option value={dt} key={dt}>
              {dt}
            </Select.Option>
          ))}
        </SelectField>
      </Col>
      <Col xs={24} sm={6}>
        <SelectField
          name="deliveryStatus"
          label="Delivery Status"
          required={true}
        >
          {Object.values(DeliveryStatus).map(ds => (
            <Select.Option value={ds} key={ds}>
              {ds}
            </Select.Option>
          ))}
        </SelectField>
      </Col>
      <Col xs={24} sm={6}>
        <SelectField
          name="paymentType"
          label="Payment Type"
          defaultValue={PaymentTypes.Cash}
          required={true}
        >
          {Object.values(PaymentTypes).map(pt => (
            <Select.Option value={pt} key={pt}>
              {pt}
            </Select.Option>
          ))}
        </SelectField>
      </Col>
      <Col xs={24} sm={6}>
        <SelectField
          name="paymentStatus"
          label="Payment Status"
          required={true}
        >
          {Object.values(PaymentStatus).map(pt => (
            <Select.Option value={pt} key={pt}>
              {pt}
            </Select.Option>
          ))}
        </SelectField>
      </Col>
    </>
  );
};

export default Details;
