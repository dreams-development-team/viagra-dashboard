import { Button, Card, Col, Empty, Row } from "antd";
import { Form as FormikForm, Formik } from "formik";
import { isBoolean, isNil } from "lodash";
import React from "react";
import { useSetState } from "react-use";
import { branch, compose, renderComponent, setDisplayName } from "recompose";
import { ProductProduct } from "src/components/elements/apollo-components";
import { InputField, TextField } from "src/components/elements/form";
import { Spinner } from "src/components/elements/spinner";
import { OrderDto } from "../../dto";
import Address from "./address";
import Cart from "./cart";
import Details from "./details";

type Props = {
  initialValues: OrderDto;
  onSubmit: (values) => Promise<void>;
  loading: boolean;
  metaLoading?: boolean;
};

const enhance = compose<Props, Props>(
  setDisplayName("src/components/views/products/elements/form"),
  branch(
    ({ metaLoading }: Props) => Boolean(metaLoading),
    renderComponent(() => <Spinner />)
  ),
  branch(
    ({ metaLoading, initialValues }: Props) =>
      isBoolean(metaLoading) && !metaLoading && isNil(initialValues),
    renderComponent(() => <Empty />)
  )
);

const Form = ({ initialValues, onSubmit, loading }: Props) => {
  const [cartProducts, setCartProduct] = useSetState<{
    [id: number]: ProductProduct;
  }>(
    initialValues.cart.reduce(
      (acc, c) => ({
        ...acc,
        [c.productId]: c.product
      }),
      {}
    )
  );

  return (
    <Card title={initialValues.id ? `Update #${initialValues.id}` : "Create"}>
      <Formik<OrderDto & { mainForm?: boolean }>
        initialValues={initialValues}
        onSubmit={({ mainForm, ...values }) =>
          mainForm && onSubmit(OrderDto.wrap(values))
        }
        render={formProps => {
          return (
            <FormikForm autoComplete="off">
              <Row gutter={30}>
                <Col xs={24} sm={8}>
                  <InputField
                    name="customerName"
                    label="Customer Name"
                    required={true}
                  />
                </Col>
                <Col xs={24} sm={8}>
                  <InputField name="phone" label="Phone" />
                </Col>
                <Col xs={24} sm={8}>
                  <InputField name="email" label="Email" />
                </Col>
                <Address />
                <Details />
                <Col xs={24} sm={24}>
                  <Cart
                    cart={formProps.values.cart}
                    products={cartProducts}
                    setCartProduct={setCartProduct}
                  />
                </Col>
                <Col xs={24} sm={24}>
                  <TextField name="comment" label="Comment" rows={6} />
                </Col>
              </Row>
              <Button
                htmlType="submit"
                disabled={loading}
                loading={loading}
                onClick={() => {
                  formProps.setFieldValue("mainForm", true);
                  formProps.submitForm();
                }}
              >
                {loading ? "Loading" : "Save"}
              </Button>
            </FormikForm>
          );
        }}
      />
    </Card>
  );
};

export default enhance(Form);
