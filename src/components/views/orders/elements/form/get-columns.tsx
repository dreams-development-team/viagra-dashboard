import { ColumnProps } from "antd/lib/table";
import { ProductProduct } from "src/components/elements/apollo-components";

export const getColumns = (): Array<ColumnProps<ProductProduct>> => [
  {
    title: "Title",
    key: "title",
    dataIndex: "title"
  },
  {
    title: "Url",
    key: "url",
    dataIndex: "url"
  }
  // TODO: add actions main action, if pending in work(in progress)
];
