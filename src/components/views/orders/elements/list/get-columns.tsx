import { Dropdown, Menu } from "antd";
import { ColumnProps } from "antd/lib/table";
import moment from "moment";
import React from "react";
import { Link } from "react-router5";
import { OrderOrder } from "src/components/elements/apollo-components";

export const getColumns = (): Array<ColumnProps<OrderOrder>> => [
  {
    title: "Number",
    key: "number",
    dataIndex: "number"
  },
  {
    title: "Delivery Status",
    key: "deliveryStatus",
    dataIndex: "deliveryStatus"
  },
  {
    title: "Created At",
    dataIndex: "createdAt",
    render: date =>
      moment(Number(date)).format("dddd, MMMM Do YYYY, h:mm:ss a"),
    sorter: true
  },
  {
    title: "Actions",
    align: "right",
    key: "actions",
    render: (_, { id }) => (
      <Dropdown.Button
        style={{ marginBottom: 40 }}
        overlay={
          <Menu>
            <Menu.Item key={1}>
              <Link routeName="home.orders.update" routeParams={{ id }}>
                Update
              </Link>
            </Menu.Item>
          </Menu>
        }
      >
        Actions
      </Dropdown.Button>
    )
  }
];
