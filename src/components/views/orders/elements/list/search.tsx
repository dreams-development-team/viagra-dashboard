import { Button, Card, Col, Row } from "antd";
import { Form, Formik } from "formik";
import React from "react";
import { compose, setDisplayName } from "recompose";
import { InputField } from "src/components/elements/form";

type Props = {
  onSubmit: (values: object) => void;
};

const enhance = compose<Props, Props>(
  setDisplayName("src/components/views/orders/elements/search")
);

const Search = ({ onSubmit }: Props) => {
  return (
    <Card title="Filter" style={{ marginBottom: 30 }}>
      <Formik
        initialValues={{}}
        onSubmit={onSubmit}
        render={formikProps => (
          <Form autoComplete="off">
            <Row gutter={20}>
              <Col xs={24} sm={12}>
                <InputField name="number" label="Number" />
              </Col>
              <Col xs={24} sm={24}>
                <Button
                  icon="search"
                  type="primary"
                  htmlType="submit"
                  style={{ marginRight: 20 }}
                >
                  Search
                </Button>
                <Button onClick={() => formikProps.resetForm()}>Reset</Button>
              </Col>
            </Row>
          </Form>
        )}
      />
    </Card>
  );
};

export default enhance(Search);
