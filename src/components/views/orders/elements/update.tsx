import React, { useCallback, useState } from "react";
import { compose, setDisplayName } from "recompose";
import {
  useOrder,
  useUpdateOrder
} from "src/components/elements/apollo-components";
import { OrderDto } from "../dto";
import Form from "./form";

type Props = {
  navigate: (routeName: string) => void;
  id: number;
};

const enhance = compose<Props, Props>(
  setDisplayName("src/components/views/orders/elements/update")
);

const Update = ({ navigate, id }: Props) => {
  const [loading, setLoading] = useState(false);
  const mutation = useUpdateOrder();
  const { data, loading: pLoading } = useOrder({
    variables: { where: { id } }
  });

  const handleSubmit = useCallback(async values => {
    try {
      setLoading(true);
      await mutation({
        variables: {
          data: values
        }
      });
      setLoading(false);

      navigate("home.orders");
    } catch {
      setLoading(false);
    }
  }, []);

  return (
    <Form
      metaLoading={pLoading}
      loading={loading}
      initialValues={OrderDto.unwrap(data!.order!)}
      onSubmit={handleSubmit}
    />
  );
};

export default enhance(Update);
