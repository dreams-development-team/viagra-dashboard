// tslint:disable:variable-name
import { Exclude, plainToClass, Type } from "class-transformer";
import {
  DosageDetailInput,
  DosageInput,
  FileInput,
  ProductAddInput,
  ProductMetaInput,
  ProductProduct
} from "src/components/elements/apollo-components";
import { FileDto } from "src/dto";

export class ProductDto implements ProductAddInput {
  public static getInitialValues() {
    return plainToClass(ProductDto, {
      title: "",
      url: "",
      meta: [ProductMetaDto.getInitialValues()],
      withAlcohol: false,
      timeToStart: "",
      workTime: "",
      logo: null,
      gallery: [],
      dosages: [ProductDosageDto.getInitialValues()],
      description: "",
      seoTitle: "",
      seoDescription: "",
      seoKeywords: []
    });
  }

  public static unwrap(value: ProductDto | ProductProduct) {
    return plainToClass(ProductDto, value);
  }

  id?: number;

  url: string;

  description: string;

  @Type(() => ProductDosageDto)
  dosages: DosageInput[];

  @Type(() => FileDto)
  gallery: FileInput[];

  @Type(() => FileDto)
  logo: FileInput;

  @Type(() => ProductMetaDto)
  meta: ProductMetaInput[];

  seoDescription: string;

  seoKeywords: string[];

  seoTitle: string;

  timeToStart: string;

  title: string;

  withAlcohol: boolean;

  workTime: string;

  @Exclude()
  __typename?: string;

  @Exclude()
  createdAt?: string;

  @Exclude()
  updatedAt?: string;
}

export class ProductMetaDto implements ProductMetaInput {
  static getInitialValues() {
    return { key: "", value: "" };
  }

  key: string;

  value: string;

  @Exclude()
  __typename?: string;
}

export class ProductDosageDto implements DosageInput {
  static getInitialValues() {
    return { dosage: "", details: [ProductDosageDetailDto.getInitialValues()] };
  }

  dosage: string;

  id?: number;

  productId?: number;

  @Type(() => ProductDosageDetailDto)
  details: DosageDetailInput[];

  @Exclude()
  __typename?: string;
}

export class ProductDosageDetailDto implements DosageDetailInput {
  static getInitialValues() {
    return {
      count: 0,
      price: 0.0
    };
  }

  count: number;

  dosageId?: number;

  price: number;

  id?: number;

  @Exclude()
  __typename?: string;
}
