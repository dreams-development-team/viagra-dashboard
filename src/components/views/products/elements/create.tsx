import React, { useCallback, useState } from "react";
import { compose, setDisplayName } from "recompose";
import {
  ProductProduct,
  useCreateProduct
} from "src/components/elements/apollo-components";
import { ProductDto } from "../dto";
import Form from "./form";

type Props = {
  navigate: (routeName: string) => void;
  duplicationData?: ProductProduct;
};

const enhance = compose<Props, Props>(
  setDisplayName("src/components/views/products/elements/create")
);

const Create = ({ navigate, duplicationData }: Props) => {
  const [loading, setLoading] = useState(false);
  const mutation = useCreateProduct();

  const handleSubmit = useCallback(async values => {
    try {
      setLoading(true);
      await mutation({
        variables: { data: values }
      });
      setLoading(false);

      navigate("home.products");
    } catch {
      setLoading(false);
    }
  }, []);

  return (
    <Form
      loading={loading}
      initialValues={
        duplicationData
          ? ProductDto.unwrap(duplicationData)
          : ProductDto.getInitialValues()
      }
      onSubmit={handleSubmit}
    />
  );
};

export default enhance(Create);
