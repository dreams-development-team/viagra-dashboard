import { Button, Col, List, Row } from "antd";
import { FieldArray } from "formik";
import React from "react";
import {
  DosageDetailInput,
  DosageInput
} from "src/components/elements/apollo-components";
import { InputField } from "src/components/elements/form";
import { ProductDosageDetailDto } from "../../dto";

const DosageDetails = ({
  details,
  dosageIndex,
  dosage
}: {
  details: DosageDetailInput[];
  dosageIndex: number;
  dosage: DosageInput;
}) => {
  return (
    <FieldArray
      name={`dosages[${dosageIndex}].details`}
      render={filedDetailsProps => {
        return (
          <List
            header={<div>Details</div>}
            footer={
              <footer style={{ textAlign: "right" }}>
                <Button
                  htmlType="button"
                  onClick={() =>
                    filedDetailsProps.push(
                      ProductDosageDetailDto.getInitialValues()
                    )
                  }
                >
                  Add Detail
                </Button>
              </footer>
            }
            bordered={true}
            dataSource={details}
            renderItem={(detail, detailIndex) => {
              return (
                <List.Item>
                  <Row
                    key={detail.id || detailIndex}
                    type="flex"
                    align="middle"
                    style={{ width: "100%" }}
                  >
                    <Col xs={24} sm={8}>
                      <InputField
                        type="number"
                        name={`dosages[${dosageIndex}].details[${detailIndex}].count`}
                        label="Count"
                        required={true}
                      />
                    </Col>
                    <Col xs={24} sm={8}>
                      <InputField
                        type="number"
                        name={`dosages[${dosageIndex}].details[${detailIndex}].price`}
                        label="Price"
                        required={true}
                      />
                    </Col>
                    {dosage.details.length > 1 && (
                      <Col xs={24} sm={8}>
                        <Button
                          htmlType="button"
                          onClick={() => filedDetailsProps.remove(detailIndex)}
                        >
                          Remove
                        </Button>
                      </Col>
                    )}
                  </Row>
                </List.Item>
              );
            }}
          />
        );
      }}
    />
  );
};

export default DosageDetails;
