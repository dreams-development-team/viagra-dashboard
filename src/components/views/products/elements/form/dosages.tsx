import { Button, Card, Col, Row } from "antd";
import { FieldArray } from "formik";
import React from "react";
import { DosageInput } from "src/components/elements/apollo-components";
import { InputField } from "src/components/elements/form";
import { ProductDosageDto } from "../../dto";
import DosageDetails from "./dosage-details";

const Dosages = ({ dosages }: { dosages: DosageInput[] }) => {
  return (
    <FieldArray
      name="dosages"
      render={filedDosagesProps => {
        return (
          <Card title="Dosages" type="inner" style={{ marginBottom: 24 }}>
            {dosages.map((dosage, dosageIndex) => (
              <Row
                gutter={30}
                key={dosage.id || dosageIndex}
                style={{ marginBottom: 40 }}
              >
                <Col xs={24} sm={6}>
                  <InputField
                    name={`dosages[${dosageIndex}].dosage`}
                    label="Dosage"
                    required={true}
                  />
                </Col>
                <Col xs={24} sm={24}>
                  <DosageDetails
                    dosageIndex={dosageIndex}
                    dosage={dosages[dosageIndex]}
                    details={dosage.details}
                  />
                </Col>

                {dosages.length > 1 && (
                  <Col xs={24} sm={24} style={{ marginTop: 25 }}>
                    <Button
                      htmlType="button"
                      onClick={() => filedDosagesProps.remove(dosageIndex)}
                    >
                      Remove
                    </Button>
                  </Col>
                )}
              </Row>
            ))}
            <Button
              htmlType="button"
              style={{ float: "right" }}
              onClick={() =>
                filedDosagesProps.push(ProductDosageDto.getInitialValues())
              }
            >
              Add Dosage
            </Button>
          </Card>
        );
      }}
    />
  );
};

export default Dosages;
