import { Button, Card, Col, Empty, Row } from "antd";
import { Form as FormikForm, Formik } from "formik";
import { isBoolean, isNil } from "lodash";
import React from "react";
import { branch, compose, renderComponent, setDisplayName } from "recompose";
import {
  CheckboxField,
  InputField,
  TextField
} from "src/components/elements/form";
import { Spinner } from "src/components/elements/spinner";
import { FileManagerContainer } from "src/components/store";
import { FileDto } from "src/dto";
import { Subscribe } from "unstated";
import { ProductDto } from "../../dto";
import Dosages from "./dosages";
import Logo from "./logo";
import Meta from "./meta";
import SeoFields from "./seo";

type Props = {
  initialValues: ProductDto;
  onSubmit: (values) => Promise<void>;
  loading: boolean;
  metaLoading?: boolean;
};

const enhance = compose<Props, Props>(
  setDisplayName("src/components/views/products/elements/form"),
  branch(
    ({ metaLoading }: Props) => Boolean(metaLoading),
    renderComponent(() => <Spinner />)
  ),
  branch(
    ({ metaLoading, initialValues }: Props) =>
      isBoolean(metaLoading) && !metaLoading && isNil(initialValues),
    renderComponent(() => <Empty />)
  )
);

const Form = ({
  initialValues: { logo, ...initialValues },
  onSubmit,
  loading
}: Props) => {
  return (
    <Card title={initialValues.id ? `Update #${initialValues.id}` : "Create"}>
      <Subscribe to={[FileManagerContainer]}>
        {(fileManager: FileManagerContainer) => {
          return (
            <Formik
              initialValues={initialValues}
              onSubmit={values =>
                onSubmit(
                  ProductDto.unwrap({
                    ...values,
                    logo: fileManager.state.logo as any
                  })
                )
              }
              render={formProps => {
                return (
                  <FormikForm autoComplete="off">
                    <Row gutter={30}>
                      <Col xs={24} sm={24}>
                        <Logo
                          logo={(fileManager.state.logo || logo) as FileDto}
                          removeFiles={file => fileManager.removeFiles(file)}
                          addFile={file => fileManager.addFile(file)}
                          upsertFile={file => fileManager.upsertFile(file)}
                        />
                      </Col>
                      <Col xs={24} sm={12}>
                        <InputField
                          name="title"
                          label="Title"
                          required={true}
                        />
                      </Col>
                      <Col xs={24} sm={12}>
                        <InputField name="url" label="Url" required={true} />
                      </Col>
                      <Col xs={24} sm={8}>
                        <InputField
                          name="timeToStart"
                          label="Time To Start"
                          required={true}
                        />
                      </Col>
                      <Col xs={24} sm={8}>
                        <InputField
                          name="workTime"
                          label="Work Time"
                          required={true}
                        />
                      </Col>
                      <Col xs={24} sm={8}>
                        <CheckboxField
                          name="withAlcohol"
                          label="With Alcohol"
                        />
                      </Col>
                      <Col xs={24} sm={24}>
                        <TextField
                          name="description"
                          label="Description"
                          rows={6}
                          required={true}
                        />
                      </Col>
                      <Col xs={24} sm={24}>
                        <Dosages dosages={formProps.values.dosages} />
                      </Col>
                      <Col xs={24} sm={24}>
                        <Meta meta={formProps.values.meta!} />
                      </Col>
                      <SeoFields />
                    </Row>
                    <Button
                      htmlType="submit"
                      disabled={loading}
                      loading={loading}
                    >
                      {loading ? "Loading" : "Save"}
                    </Button>
                  </FormikForm>
                );
              }}
            />
          );
        }}
      </Subscribe>
    </Card>
  );
};

export default enhance(Form);
