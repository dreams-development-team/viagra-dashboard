import React from "react";
import { useMount, useUnmount } from "react-use";
import { useUpload } from "src/components/elements/apollo-components";
import { FormItem } from "src/components/elements/form";
import Uploader from "src/components/elements/uploader";
import { FileManagerContainer } from "src/components/store";
import { FileDto } from "src/dto";

const Logo = ({
  logo,
  removeFiles,
  addFile,
  upsertFile
}: {
  logo?: FileDto;
  removeFiles: typeof FileManagerContainer.prototype.removeFiles;
  addFile: typeof FileManagerContainer.prototype.addFile;
  upsertFile: typeof FileManagerContainer.prototype.upsertFile;
}) => {
  const uploadMutation = useUpload();

  useMount(() => {
    if (logo) {
      addFile({
        key: "logo",
        data: logo
      });
    }
  });
  useUnmount(() => removeFiles({ key: "logo" }));

  return (
    <FormItem label="Logo" required={true}>
      <Uploader
        file={logo}
        listType="picture-card"
        onRemove={() => {
          removeFiles({
            key: "logo"
          });
        }}
        onUpload={async file => {
          addFile({
            key: "logo",
            data: {
              uid: file.uid,
              url: URL.createObjectURL(file),
              name: file.name,
              status: "uploading"
            }
          });

          const { data } = await uploadMutation({
            variables: { file }
          });

          upsertFile({
            key: "logo",
            data: {
              url: data!.upload,
              status: "done"
            }
          });
        }}
      />
    </FormItem>
  );
};

export default Logo;
