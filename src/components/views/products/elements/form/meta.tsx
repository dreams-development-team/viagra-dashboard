import { Button, Card, Col, Row } from "antd";
import { FieldArray } from "formik";
import React from "react";
import { InputField } from "src/components/elements/form";
import { ProductMetaDto } from "../../dto";

const Meta = ({ meta }: { meta: ProductMetaDto[] }) => {
  return (
    <FieldArray
      name="meta"
      render={fieldProps => {
        return (
          <Card type="inner" title="Meta" style={{ marginBottom: 24 }}>
            {meta.map((_, mi) => (
              <Row gutter={30} type="flex" key={mi} align="middle">
                <Col xs={24} sm={10}>
                  <InputField
                    name={`meta[${mi}].key`}
                    label="Key"
                    required={true}
                  />
                </Col>
                <Col xs={24} sm={10}>
                  <InputField
                    name={`meta[${mi}].value`}
                    label="value"
                    required={true}
                  />
                </Col>
                {meta.length > 1 && (
                  <Col xs={24} sm={4}>
                    <Button
                      htmlType="button"
                      onClick={() => fieldProps.remove(mi)}
                    >
                      Remove
                    </Button>
                  </Col>
                )}
              </Row>
            ))}
            <Button
              htmlType="button"
              style={{ float: "right" }}
              onClick={() => fieldProps.push(ProductMetaDto.getInitialValues())}
            >
              Add Meta
            </Button>
          </Card>
        );
      }}
    />
  );
};

export default Meta;
