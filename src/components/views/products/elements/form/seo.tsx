import { Col } from "antd";
import React from "react";
import {
  InputField,
  SelectField,
  TextField
} from "src/components/elements/form";

const SeoFields = () => {
  return (
    <>
      <Col xs={24} sm={12}>
        <InputField name="seoTitle" label="Seo Title" required={true} />
      </Col>
      <Col xs={24} sm={12}>
        <SelectField
          required={true}
          name="seoKeywords"
          label="Seo Keywords"
          mode="tags"
        />
      </Col>
      <Col xs={24} sm={24}>
        <TextField
          name="seoDescription"
          label="Seo Description"
          rows={5}
          required={true}
        />
      </Col>
    </>
  );
};

export default SeoFields;
