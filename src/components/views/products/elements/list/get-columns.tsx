import { Dropdown, Menu } from "antd";
import { ColumnProps } from "antd/lib/table";
import moment from "moment";
import React from "react";
import { Link } from "react-router5";
import { ProductProduct } from "src/components/elements/apollo-components";

export const getColumns = ({
  remove
}: {
  remove: any;
}): Array<ColumnProps<ProductProduct>> => [
  {
    title: "Title",
    key: "title",
    dataIndex: "title"
  },
  {
    title: "Url",
    key: "url",
    dataIndex: "url"
  },
  {
    title: "Created At",
    dataIndex: "createdAt",
    render: date =>
      moment(Number(date)).format("dddd, MMMM Do YYYY, h:mm:ss a"),
    sorter: true
  },
  {
    title: "Actions",
    align: "right",
    key: "actions",
    render: (_, { id, ...product }) => (
      <Dropdown.Button
        style={{ marginBottom: 40 }}
        overlay={
          <Menu>
            <Menu.Item key={1}>
              <Link routeName="home.products.update" routeParams={{ id }}>
                Update
              </Link>
            </Menu.Item>
            <Menu.Item key={2}>
              <Link routeName="home.products.create" routeParams={{ product }}>
                Duplicate
              </Link>
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item key={3}>
              <span onClick={() => remove(Number(id))}>Remove</span>
            </Menu.Item>
          </Menu>
        }
      >
        Actions
      </Dropdown.Button>
    )
  }
];
