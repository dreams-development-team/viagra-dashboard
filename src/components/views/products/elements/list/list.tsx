import { Button } from "antd";
import React, { useCallback } from "react";
import { Link, withRoute } from "react-router5";
import { compose, setDisplayName } from "recompose";
import { useRemoveProduct, useSearchProducts } from "src/components/elements/apollo-components";
import Table from "src/components/elements/table";
import { getOffset } from "src/components/elements/table/lib";
import { useSearchState } from "src/components/hooks";
import { searchProductsQuery } from "src/graphql/products.graphql";
import { getColumns } from "./get-columns";
import Search from "./search";

const enhance = compose(
  setDisplayName("src/components/views/products/elements/list"),
  withRoute
);

const List = () => {
  const { data, loading, refetch } = useSearchProducts({
    variables: {},
    notifyOnNetworkStatusChange: true,
    fetchPolicy: "network-only"
  });
  const { search, setSearch } = useSearchState();
  const removeMutation = useRemoveProduct();

  const handleSearch = useCallback(async values => {
    setSearch({
      currentPage: 1,
      variables: { where: values, skip: 0 }
    });

    await refetch({ where: values, skip: 0 });
  }, []);

  return (
    <>
      <nav style={{ marginBottom: 40 }}>
        <Link routeName="home.products.create">
          <Button type="primary" icon="plus" size={"large"}>
            Create
          </Button>
        </Link>
      </nav>
      <Search onSubmit={handleSearch} />
      <Table
        bordered={true}
        rowKey="id"
        loading={loading}
        dataSource={data!.searchProducts && data!.searchProducts.data}
        pagination={{
          total: data!.searchProducts && data!.searchProducts.count,
          current: search.currentPage,
          defaultPageSize: 20,
          style: { marginRight: 20 },
          async onChange(cp, s = 0) {
            setSearch({ currentPage: cp });

            await refetch({ skip: getOffset(cp, s) });
          }
        }}
        columns={getColumns({
          remove: async (id: number) => {
            await removeMutation({
              variables: { where: { id } },
              refetchQueries: [
                {
                  query: searchProductsQuery,
                  variables: search.variables
                }
              ]
            });
          }
        })}
      />
    </>
  );
};

export default enhance(List);
