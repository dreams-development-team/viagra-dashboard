import React, { useCallback, useState } from "react";
import { compose, setDisplayName } from "recompose";
import {
  useProduct,
  useUpdateProduct
} from "src/components/elements/apollo-components";
import { ProductDto } from "../dto";
import Form from "./form";

type Props = {
  navigate: (routeName: string) => void;
  id: number;
};

const enhance = compose<Props, Props>(
  setDisplayName("src/components/views/products/elements/update")
);

const Update = ({ navigate, id }: Props) => {
  const [loading, setLoading] = useState(false);
  const mutation = useUpdateProduct();
  const { data, loading: pLoading } = useProduct({
    variables: { where: { id } }
  });

  const handleSubmit = useCallback(async values => {
    try {
      setLoading(true);
      await mutation({
        variables: {
          data: values
        }
      });
      setLoading(false);

      navigate("home.products");
    } catch {
      setLoading(false);
    }
  }, []);

  return (
    <Form
      metaLoading={pLoading}
      loading={loading}
      initialValues={ProductDto.unwrap(data!.product!)}
      onSubmit={handleSubmit}
    />
  );
};

export default enhance(Update);
