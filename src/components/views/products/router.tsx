import React from "react";
import { routeNode } from "react-router5";
import { RouteContext } from "react-router5/types/types";
import { compose, setDisplayName } from "recompose";
import { endsWithSegment } from "router5-helpers";
import Create from "./elements/create";
import List from "./elements/list";
import Update from "./elements/update";

const enhance = compose<any, any>(
  setDisplayName("src/components/views/products/router"),
  routeNode("home.products")
);

type Props = RouteContext;

const Router = ({ route, router: { navigate } }: Props) => {
  if (endsWithSegment(route, "create")) {
    return (
      <Create navigate={navigate} duplicationData={route.params.product} />
    );
  }

  if (endsWithSegment(route, "update")) {
    return <Update navigate={navigate} id={Number(route.params.id)} />;
  }

  return <List />;
};

export default enhance(Router);
