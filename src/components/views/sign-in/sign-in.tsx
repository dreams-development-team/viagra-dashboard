import styled from "@emotion/styled";
import { Button, Card, Col, Row } from "antd";
import { Form, Formik } from "formik";
import React, { useCallback, useState } from "react";
import { routeNode } from "react-router5";
import { compose } from "recompose";
import {
  useAuthUser,
  useSignIn
} from "src/components/elements/apollo-components";
import { FormItem, InputField } from "src/components/elements/form";
import store from "store";

const enhance = compose<any, any>(routeNode("signIn"));

const Wrapper = styled.main({
  width: "50vw",
  margin: "0 auto",
  paddingTop: "200px"
});

const SignIn = () => {
  const [loading, setLoading] = useState(false);
  const signIn = useSignIn();
  const { refetch } = useAuthUser({ skip: true });

  const handleSubmit = useCallback(async values => {
    try {
      setLoading(true);
      const res = await signIn({
        variables: values
      });
      setLoading(false);

      store.set("token", res.data!.signIn!.token);
      await refetch();
    } catch {
      setLoading(false);
    }
  }, []);

  return (
    <Wrapper>
      <Card title="Sign In">
        <Formik
          initialValues={{}}
          onSubmit={handleSubmit}
          render={() => (
            <Form autoComplete="off">
              <InputField name="login" label="Login" required={true} />
              <InputField
                type="password"
                name="password"
                label="Password"
                required={true}
              />
              <FormItem>
                <Row gutter={15}>
                  <Col xs={24} sm={3}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={loading}
                      disabled={loading}
                    >
                      Sing In
                    </Button>
                  </Col>
                </Row>
              </FormItem>
            </Form>
          )}
        />
      </Card>
    </Wrapper>
  );
};

export default enhance(SignIn);
