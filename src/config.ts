const DEFAULT_ENV = "development";

export const ENV = process.env.REACT_APP_ENV || DEFAULT_ENV;
export const IS_DEV = ENV === "development";
export const IS_PROD = ENV === "production";
export const IS_TEST = ENV === "test";

export const API_URI = process.env.REACT_APP_API_URI;
