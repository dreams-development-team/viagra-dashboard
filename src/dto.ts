// tslint:disable:variable-name
import { UploadFileStatus } from "antd/lib/upload/interface";
import { Exclude } from "class-transformer";
import { FileInput } from "./components/elements/apollo-components";

export class FileDto implements FileInput {
  uid?: string;

  name?: string;

  url: string;

  @Exclude()
  __typename?: string;

  @Exclude()
  response?: any;

  @Exclude()
  status?: UploadFileStatus;
}
