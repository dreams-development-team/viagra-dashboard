import gql from "graphql-tag";

export const signInMutation = gql`
  mutation SignIn($login: String!, $password: String!) {
    signIn(password: $password, login: $login) {
      token
    }
  }
`;

export const authUserQuery = gql`
  query AuthUser {
    authUser {
      id
      login
    }
  }
`;
