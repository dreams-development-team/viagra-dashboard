import gql from "graphql-tag";

export const uploadFileMutation = gql`
  mutation Upload($file: Upload!) {
    upload(file: $file)
  }
`;
