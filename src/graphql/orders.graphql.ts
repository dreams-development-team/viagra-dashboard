import gql from "graphql-tag";

export const searchOrdersQuery = gql`
  query SearchOrders($where: OrdersWhereInput, $skip: Float, $take: Float) {
    searchOrders(where: $where, skip: $skip, take: $take) {
      count
      data {
        id
        number
        deliveryType
        deliveryStatus
        paymentType
        paymentStatus
        customerName
        phone
        comment
        email
        createdAt
        updatedAt
        address {
          city
          street
          apartment
          postalCode
        }
        cart {
          id
          productId
          dosageId
          dosageDetailId
          quantity
          product {
            id
            title
            url
            description
            logo {
              url
            }
            dosages {
              id
              dosage
              details {
                id
                count
                price
              }
            }
          }
          dosage {
            id
            dosage
          }
          dosageDetail {
            id
            count
            price
          }
        }
      }
    }
  }
`;

export const createOrderMutation = gql`
  mutation CreateOrder($data: OrderAddInput!) {
    createOrder(data: $data) {
      id
      number
      deliveryType
      deliveryStatus
      paymentType
      paymentStatus
      customerName
      phone
      comment
      email
      createdAt
      updatedAt
      address {
        city
        street
        apartment
        postalCode
      }
      cart {
        id
        productId
        dosageId
        dosageDetailId
        quantity
        product {
          id
          title
          url
          description
          logo {
            url
          }
          dosages {
            id
            dosage
            details {
              id
              count
              price
            }
          }
        }
        dosage {
          id
          dosage
        }
        dosageDetail {
          id
          count
          price
        }
      }
    }
  }
`;

export const orderQuery = gql`
  query Order($where: OrderWhereInput!) {
    order(where: $where) {
      id
      number
      deliveryType
      deliveryStatus
      paymentType
      paymentStatus
      customerName
      phone
      comment
      email
      createdAt
      updatedAt
      address {
        city
        street
        apartment
        postalCode
      }
      cart {
        id
        productId
        dosageId
        dosageDetailId
        quantity
        product {
          id
          title
          url
          description
          logo {
            url
          }
          dosages {
            id
            dosage
            details {
              id
              count
              price
            }
          }
        }
        dosage {
          id
          dosage
        }
        dosageDetail {
          id
          count
          price
        }
      }
    }
  }
`;

export const updateOrderMutation = gql`
  mutation UpdateOrder($data: OrderUpdateInput!) {
    updateOrder(data: $data) {
      id
      number
      deliveryType
      deliveryStatus
      paymentType
      paymentStatus
      customerName
      phone
      comment
      email
      createdAt
      updatedAt
      address {
        city
        street
        apartment
        postalCode
      }
      cart {
        id
        productId
        dosageId
        dosageDetailId
        quantity
        product {
          id
          title
          url
          description
          logo {
            url
          }
          dosages {
            id
            dosage
            details {
              id
              count
              price
            }
          }
        }
        dosage {
          id
          dosage
        }
        dosageDetail {
          id
          count
          price
        }
      }
    }
  }
`;
