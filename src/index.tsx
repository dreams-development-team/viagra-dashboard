import React from "react";
import { ApolloProvider } from "react-apollo-hooks";
import ReactDOM from "react-dom";
import { RouterProvider } from "react-router5";
import "reflect-metadata";
import App from "src/components/elements/app";
import ThemeLayout from "src/components/elements/theme-layout";
import registerServiceWorker from "src/registerServiceWorker";
import { createApolloClient, router } from "src/utils";
import { Provider } from "unstated";

router.start(() => {
  ReactDOM.render(
    <ApolloProvider client={createApolloClient({ router })}>
      <RouterProvider router={router}>
        <ThemeLayout>
          <Provider>
            <App />
          </Provider>
        </ThemeLayout>
      </RouterProvider>
    </ApolloProvider>,
    document.getElementById("root")
  );

  registerServiceWorker();
});
