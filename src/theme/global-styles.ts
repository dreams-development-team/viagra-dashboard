import "antd/dist/antd.css";

export const globalStyles = {
  "html, body": {
    margin: 0,
    padding: 0,
    fontSize: "100%",
    minHeight: "100vh",
    maxWidth: "100vw"
  },

  "#root": {
    height: "100%"
  },

  a: {
    textDecoration: "none",
    color: "inherit"
  }
};
