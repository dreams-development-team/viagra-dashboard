import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloClient } from "apollo-client";
import { ApolloLink } from "apollo-link";
import { setContext } from "apollo-link-context";
import { onError } from "apollo-link-error";
import { ServerError } from "apollo-link-http-common";
import { createUploadLink } from "apollo-upload-client";
import { Router } from "router5";
import { API_URI } from "src/config";
import store from "store";

const authMiddleware = setContext((_, { headers }) => {
  const token = store.get("token");
  const _headers = { ...headers };

  if (token) {
    _headers.Authorization = `Bearer ${token}`;
  }

  return {
    headers: _headers
  };
});

export const createApolloClient = ({ router }: { router: Router }) =>
  new ApolloClient({
    link: ApolloLink.from([
      onError(({ graphQLErrors, networkError }) => {
        if (graphQLErrors) {
          graphQLErrors.map(({ message, locations, path, extensions }) => {
            console.error(
              `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
            );

            if (extensions && extensions.code === "UNAUTHENTICATED") {
              store.remove("token");

              router.navigate("signIn");
            }
          });
        }

        if (networkError) {
          // TODO: handle 5** and unknown errors
          console.log((networkError as ServerError).statusCode);
          console.error(`[Network error]: ${networkError}`);
        }
      }),
      authMiddleware,
      createUploadLink({
        uri: API_URI
      })
    ]),
    cache: new InMemoryCache()
  });
