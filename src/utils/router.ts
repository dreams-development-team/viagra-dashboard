import { createRouter, Route } from "router5";
import browserPlugin from "router5-plugin-browser";
import loggerPlugin from "router5-plugin-logger";

const routes: Route[] = [
  {
    name: "home",
    path: "/",
    children: [
      {
        name: "categories",
        path: "categories",
        children: [
          {
            name: "create",
            path: "/create"
          },
          {
            name: "update",
            path: "/update/:id"
          }
        ]
      },
      {
        name: "products",
        path: "products",
        children: [
          {
            name: "create",
            path: "/create"
          },
          {
            name: "update",
            path: "/update/:id"
          }
        ]
      },
      {
        name: "orders",
        path: "orders",
        children: [
          {
            name: "create",
            path: "/create"
          },
          {
            name: "update",
            path: "/update/:id"
          }
        ]
      }
    ]
  },
  {
    name: "signIn",
    path: "/sign-in"
  },
];

export const router = createRouter(routes, {
  queryParamsMode: "strict"
});

router.usePlugin(browserPlugin());
router.usePlugin(loggerPlugin);
